# dai16085 & dai16132

from sys import argv, exit
from math import pi as mathPi
import multiprocessing
import time

'''This program calculate process using process, each process calculate a part of the integral.
Total_time is a shared variable in which each process add its result. User gives the number of steps'''


def calculate_pi(start, stop, l, step):

    l.acquire()

    sum = 0
    for i in range(int(start), int(stop), step):
        x = (i + 0.5) * step
        sum += 4.0 / (1.0 + x ** 2)

    sum = sum * step

    total_sum.value = total_sum.value + sum

    l.release()


if __name__ == "__main__":
    if len(argv) != 2:
        print('Usage: {} <number of steps>' .format(argv[0]))
        exit(1)

    numberOfSteps = argv[1]

    try:
       numberOfSteps = int(numberOfSteps)

    except ValueError as e:
        print('Integer convertion error: {}' .format(e))
        exit(2)

    if numberOfSteps <= 0:
        print('Steps cannot be non-positive.')
        exit(3)

    num_of_threads = multiprocessing.cpu_count()
    print("The number of CPU threads is :", num_of_threads)

    # shared variable process safe
    total_sum = multiprocessing.Value('f', 0)

    l = multiprocessing.Lock()

    start_time = time.time()

    step = round(1.0 / num_of_threads) + 1
    for i in range(num_of_threads):
        start = step * i
        stop = start + (numberOfSteps / num_of_threads)
        if i == numberOfSteps - 1:
            stop += numberOfSteps % num_of_threads

        p = multiprocessing.Process(target=calculate_pi, args=(start, stop, l, step))
        p.start()
        p.join()

    total_time = time.time() - start_time

    print('Sequential program results with {} steps'.format(numberOfSteps))
    print('Computed pi = {}'.format(total_sum.value))
    print('Difference between estimated pi and math.pi = {}'.format(abs(total_sum.value - mathPi)))
    print('Time to compute = {} seconds'.format(total_time))





